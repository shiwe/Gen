# Importing necessary libraries
import os

def search_string_in_files(directory, search_string, output_file, extensions=('.py', '.cpp', '.c', '.java', '.dec')):
    """
    Search for a given string in files with specified extensions under a directory and save the file names
    where the string is found to a text file.

    Parameters:
    - directory (str): The directory path to search in.
    - search_string (str): The string to search for in the files.
    - output_file (str): The file path where to save the names of the files containing the search string.
    - extensions (tuple): The file extensions to include in the search.
    """
    matched_files = []

    # Walking through the directory
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(extensions):
                file_path = os.path.join(root, file)
                # Searching for the string in the file
                try:
                    with open(file_path, 'r', encoding='utf-8') as f:
                        if search_string in f.read():
                            matched_files.append(file_path)
                except Exception as e:
                    print(f"Error reading file {file_path}: {e}")

    # Writing the matched file paths to the output file
    with open(output_file, 'w', encoding='utf-8') as f:
        for file in matched_files:
            f.write(f"{file}\n")

# Example usage (Commented out to prevent execution here)
# directory_to_search = "/path/to/your/directory"
# search_string = "Powheg"
# output_file_path = "found_files.txt"
# search_string_in_files(directory_to_search, search_string, output_file_path)

directory_to_search = "../../Gen/DecFiles/dkfiles/"
search_string = "powheg"
output_file_path = "found_powheg.txt"
search_string_in_files(directory_to_search, search_string, output_file_path)
