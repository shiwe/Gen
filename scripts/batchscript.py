# Redefining the updated function to include copying header lines before processing variable lines

def migrate(input_file, output_file):

    key1 = 0
    key2 = 1
    commands_list = ['"lhans1 10770"', '"lhans2 10770"']

    # Open the original file to read
    with open(input_file, 'r') as file:
        lines = file.readlines()
    
    # Open the output file to write
    with open(output_file, 'w') as file:
        # Copy header lines
        for line in lines:
            if line.strip().startswith('# NickName'):
                start = line.find(':')
                end = line.find('_')
                prcs = line[start+1:end].strip()
                if prcs == "Higgs":
                    prcs = "gg_H_quark-mass-effects"
                prcs_str = '"' + prcs + '"'
            if line.strip() == '# InsertPythonCode:':
                key1 = 1
                file.write(line)
            if line.strip() == '# EndInsertPythonCode':
                key1 = 0
            if key1 == 1:
                if line.strip().startswith('# from Configurables import LoKi__FullGenEventCut'):
                    file.write(line)
                    key1 = 0
                    continue
                if line.strip().startswith('# from '):
                    file.write(line)
                    key2 = 0
                    continue
                if key2 == 0:
                    piuptool = '#\n# Generation(genAlgName).PileUpTool           = "FixedLuminosityForRareProcess"\n'
                    file.write(piuptool)
                    key2 = 1
                if line.strip().startswith('# Generation(genAlgName).Special.PowhegProduction.Commands'):
                    # Extract commands
                    start = line.find('[')
                    end = line.find(']')
                    commands_str = line[start+1:end].strip()
                    commands_list.extend([cmd.strip() for cmd in commands_str.split(',')]) 
                    commands_formatted = ',\n#                 '.join(commands_list)
                    sampleGenToolsOpts_str = f'#\n# sampleGenToolsOpts = {{\n#    "Commands": [\n#                 {commands_formatted}\n#                ],\n#    "Process" : {prcs_str}\n# }}\n'
                    file.write(sampleGenToolsOpts_str)
                    updateopts = '#\n# Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)\n'
                    file.write(updateopts)
                continue
            if line.strip().startswith('# FullEventCuts'):
                file.write(line)
                phgoption = '# Production: Powheg\n'
                file.write(phgoption)
                continue
            if line.strip().startswith('# ExtraOptions'):
                continue
            file.write(line)

import os
def process(sourcedir, targetdir):
    if not os.path.exists(targetdir):
        os.makedirs(targetdir)

    failures = 0
    for filename in os.listdir(sourcedir):
        source_file = os.path.join(sourcedir, filename)
        target_file = os.path.join(targetdir, filename)

        if os.path.isfile(source_file):
            try:
                migrate(source_file, target_file)
                print(f"Processed {filename}")
            except Exception as e:
                print(f"Failed to process {filename}: {e}")
    return failures


# To maintain the policy of commenting out function calls during development:
import sys
if len(sys.argv) != 3:
    print("Usage: python migrate.py inputfile.dec")
    sys.exit(1)
else:
    failures = process(sys.argv[1], sys.argv[2])
    if failures > 0:
        print(f"Migration completed with {failures} failures.")
    else:
        print("Migration completed successfully for all files.")
