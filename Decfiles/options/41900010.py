# file ../options/41900010.py generated: Wed, 20 Mar 2024 10:13:34
#
# Event Type: 41900010
#
# ASCII decay Descriptor: pp -> ([W+ -> l nu_l]cc) (Z0 -> b b~) ...
#
genAlgName="Generation"
from Configurables import Generation
Generation(genAlgName).EventType = 41900010
Generation(genAlgName).SampleGenerationTool = "Special"
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Powheg" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/tt_bb=1l,10GeV,2b,powheg.dec"
from Configurables import Special
Generation(genAlgName).addTool( Special )
Generation(genAlgName).Special.CutTool = ""
Generation(genAlgName).FullGenEventCutTool = "LoKi::FullGenEventCut/HiggsTypeCut"

from Configurables import Special, Pythia8Production
sampleGenToolsOpts = {
    "Commands": [
                 "lhans1 10770",
                 "lhans2 10770",
                 "topdecaymode 02000"
                ]
    "Process" : "ttb_NLO_dec"
}

Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)

Generation(genAlgName).PileUpTool           = "FixedLuminosityForRareProcess"
from Configurables import LoKi__FullGenEventCut
Generation(genAlgName).addTool( LoKi__FullGenEventCut, "HiggsTypeCut" )
tracksInAcc = Generation(genAlgName).HiggsTypeCut
tracksInAcc.Code = " ((count ( isGoodLeptonW ) >0) & (count ( isGoodBeauty)>1)) "
tracksInAcc.Preambulo += [                                                                        
     "from GaudiKernel.SystemOfUnits import  GeV, mrad"                                           
    , "isGoodLeptonW     = ((  'W+' == GABSID ) & GINTREE( GLEPTON & ( GTHETA < 350.0*mrad ) & (GPT > 10*GeV)))"
    , "isGoodBeauty   = ((  'b' == GABSID ) & GINTREE( GBEAUTY & ( GTHETA < 350.0*mrad ) & (GPT > 0*GeV)))"
   ]      
Generation(genAlgName).Special.PileUpProductionTool = "Pythia8Production"
Generation(genAlgName).PileUpTool = "FixedLuminosityForRareProcess"
Generation(genAlgName).Special.addTool( Pythia8Production () )
Generation(genAlgName).Special.ReinitializePileUpGenerator = False
Generation(genAlgName).Special.Pythia8Production.Tuning = "LHCbDefault.cmd"

