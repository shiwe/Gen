# file ../options/40114050.py generated: Wed, 20 Mar 2024 10:12:32
#
# Event Type: 40114050
#
# ASCII decay Descriptor: pp => ( Higgs0 => (Z0 => mu+ mu-) (Z0 => mu+ mu-) )
#
genAlgName="Generation"
from Configurables import Generation
Generation(genAlgName).EventType = 40114050
Generation(genAlgName).SampleGenerationTool = "Special"
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Powheg" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Higgs_ZZ_mumumumu.dec"
from Configurables import Special
Generation(genAlgName).addTool( Special )
Generation(genAlgName).Special.CutTool = ""
Generation(genAlgName).FullGenEventCutTool = "LoKi::FullGenEventCut/ParsInAcc"

from Configurables import Generation, Special, PowhegProduction, Pythia8Production

Generation(genAlgName).PileUpTool           = "FixedLuminosityForRareProcess"

sampleGenToolsOpts = {
   "Commands": [
                "lhans1 10770",
                "lhans2 10770",
                "hfact    104.16d0",
                "runningscale 0",
                "massren 0",
                "zerowidth 1",
                "ew 1",
                "model 0",
                "gfermi 0.116637D-04",
                "hdecaymode -1",
                "masswindow 10d0",
                "hmass 125",
                "hwidth 3.605D-03",
                "topmass 172.5",
                "bottommass 4.75d0",
                "hdecaywidth 0"
               ],
   "Process" : "gg_H_quark-mass-effects"
}

Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import LoKi__FullGenEventCut
Generation(genAlgName).addTool( LoKi__FullGenEventCut, "ParsInAcc" )
tracksInAcc = Generation(genAlgName).ParsInAcc
tracksInAcc.Code = " ( ( count ( isGoodLepton ) > 3 ) ) "
tracksInAcc.Preambulo += [
     "from GaudiKernel.SystemOfUnits import  GeV, mrad"
   , "isGoodLepton   = ( ( 13 == GABSID ) & GCHARGED & ( GTHETA < 400.0*mrad ) & (GTHETA > 10.0*mrad) & ( GPT > 2.*GeV ) )"
   ]

Generation(genAlgName).Special.addTool( Pythia8Production )
Generation(genAlgName).Special.Pythia8Production.Commands += [ "25:onMode = off",
"25:onIfMatch = 23 23", "23:onMode = off", "23:onIfMatch = 13 -13" ]

