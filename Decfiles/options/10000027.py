# file ../options/10000027.py generated: Wed, 20 Mar 2024 10:13:31
#
# Event Type: 10000027
#
# ASCII decay Descriptor: pp -> [<Xb>]cc ...
#
genAlgName="Generation"
from Configurables import Generation
Generation(genAlgName).EventType = 10000027
Generation(genAlgName).SampleGenerationTool = "Special"
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Powheg" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/incl_b,powheg.dec"
from Configurables import Special
Generation(genAlgName).addTool( Special )
Generation(genAlgName).Special.CutTool = ""
Generation(genAlgName).FullGenEventCutTool = "LoKi::FullGenEventCut/twobinAcc"

from Configurables import Special, Pythia8Production

sampleGenToolsOpts = {
    "Commands": [
                 "lhans1 10550",
                 "lhans2 10550",
                 "qmass 4.75"
                ],
    "Process" : "incl_b"
}
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)

Generation(genAlgName).PileUpTool           = "FixedLuminosityForRareProcess"
from Configurables import LoKi__FullGenEventCut
Generation(genAlgName).addTool( LoKi__FullGenEventCut, "twobinAcc" )
tracksInAcc = Generation(genAlgName).twobinAcc
tracksInAcc.Code = " (count ( isGoodB)>1) "
tracksInAcc.Preambulo += [                                                                        
     "from GaudiKernel.SystemOfUnits import  GeV, mrad"                                           
    , "isGoodB   = ((  'b' == GABSID ) & GINTREE( GBEAUTY & ( GTHETA < 350.0*mrad ) & (GPT > 0*GeV)))"
   ]      
Generation(genAlgName).Special.PileUpProductionTool = "Pythia8Production"
Generation(genAlgName).PileUpTool = "FixedLuminosityForRareProcess"
Generation(genAlgName).Special.addTool( Pythia8Production () )
Generation(genAlgName).Special.ReinitializePileUpGenerator = False
Generation(genAlgName).Special.Pythia8Production.Tuning = "LHCbDefault.cmd"

