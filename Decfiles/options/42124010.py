# file ../options/42124010.py generated: Wed, 20 Mar 2024 10:11:04
#
# Event Type: 42124010
#
# ASCII decay Descriptor: pp => (Z0 => mu+ mu-) (Z0 => e+ e-)
#
genAlgName="Generation"
from Configurables import Generation
Generation(genAlgName).EventType = 42124010
Generation(genAlgName).SampleGenerationTool = "Special"
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Powheg" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/ZZ_mumuee.dec"
from Configurables import Special
Generation(genAlgName).addTool( Special )
Generation(genAlgName).Special.CutTool = ""
Generation(genAlgName).FullGenEventCutTool = "LoKi::FullGenEventCut/ParsInAcc"

from Configurables import Generation, Special, PowhegProduction

Generation(genAlgName).PileUpTool           = "FixedLuminosityForRareProcess"

sampleGenToolsOpts = {
   "Commands": [
                "lhans1 10770",
                "lhans2 10770",
                "mllmin 3.d0",
                "e-mu  1"
               ],
   "Process" : "ZZ"
}

Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import LoKi__FullGenEventCut
Generation(genAlgName).addTool( LoKi__FullGenEventCut, "ParsInAcc" )
tracksInAcc = Generation(genAlgName).ParsInAcc
tracksInAcc.Code = " ( ( count ( isGoodLepton ) > 3 ) ) "
tracksInAcc.Preambulo += [
     "from GaudiKernel.SystemOfUnits import  GeV, mrad"
   , "isGoodLepton   = ( ( ( 13 == GABSID ) | ( 11 == GABSID )) & GCHARGED & ( GTHETA < 400.0*mrad ) & (GTHETA > 10.0*mrad) & ( GPT > 2.*GeV ) )"
   ]

