# file ../options/42122005.py generated: Wed, 20 Mar 2024 10:13:27
#
# Event Type: 42122005
#
# ASCII decay Descriptor: pp -> (Z0/gamma* -> e+ e-) ...
#
genAlgName="Generation"
from Configurables import Generation
Generation(genAlgName).EventType = 42122005
Generation(genAlgName).SampleGenerationTool = "Special"
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Powheg" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Z_ee_PowHeg40GeV.dec"
from Configurables import Special
Generation(genAlgName).addTool( Special )
Generation(genAlgName).Special.CutTool = ""
Generation(genAlgName).FullGenEventCutTool = "LoKi::FullGenEventCut/ParsInAcc"

from Configurables import Generation, Special, PowhegProduction

Generation(genAlgName).PileUpTool           = "FixedLuminosityForRareProcess"

sampleGenToolsOpts = {
   "Commands": [
                "lhans1 10770",
                "lhans2 10770",
                "vdecaymode 1",
                "mass_low  40"
               ],
   "Process" : "Z"
}

Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import LoKi__FullGenEventCut
Generation(genAlgName).addTool( LoKi__FullGenEventCut, "ParsInAcc" )
tracksInAcc = Generation(genAlgName).ParsInAcc
tracksInAcc.Code = " ( ( count ( isGoodLepton ) > 0 ) ) "
tracksInAcc.Preambulo += [
     "from GaudiKernel.SystemOfUnits import  GeV, mrad"
   , "isGoodLepton   = ( ( 11 == GABSID ) & GCHARGED & ( GTHETA < 400.0*mrad ) & (GTHETA > 10.0*mrad) & ( GPT > 4.*GeV ) )"
   ]

